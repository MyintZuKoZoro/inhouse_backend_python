"""inhouse_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('api/admin', admin.site.urls),
    path('api/department', include('main.api.department.urls')),
    path('api/employee', include('main.api.employee.urls')),
    path('api/holiday', include('main.api.holiday.urls')),
    path('api/leave_type', include('main.api.leave_type.urls')),
    path('api/leave', include('main.api.leave.urls')),
    path('api/leave_allowance', include('main.api.leave_allowance.urls')),
    path('api/auth/', include('authentication.urls')),
    path('api/supervisor', include('main.api.supervisor.urls')),
    path('api/overtime', include('main.api.overtime.urls')),
    path('api/attendance', include('main.api.attendance.urls'))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
