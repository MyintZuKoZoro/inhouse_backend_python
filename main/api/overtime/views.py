from calendar import monthrange

from django.http import JsonResponse

from rest_framework.authentication import TokenAuthentication

from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from main.response import ApiResponse
from main.util.CommonUtil import *
import datetime
from django.utils import timezone

from main.util.ExceptionUtil import InternalServerError, BadRequest, NotFound
from main.util.ResponseUtil import ResponseUtil
from django.core.mail import send_mail


@api_view(['POST'])
def requestOvertime(request):
    try:
        current_emp = get_user_from_token(request)
        request_body = JSONParser().parse(request)
        employee = Employee.objects.get(pk=request_body.get('emp').get(
            'id')) if request_body.get('emp') is not None else current_emp
        request_emp = Employee.objects.get(
            pk=request_body['request_emp']['id'])
        # check authroized check requst time and date is in the same date check work off day
        # if not work off day check time overlap
        if employee == request_emp:
            raise BadRequest(message="You cannot send Overtime to yourself")
        overtime_serializer = OvertimeSerializer(data=request_body)
        if overtime_serializer.is_valid():
            ot_serializer_result = overtime_serializer.validated_data
            request_start_datetime = ot_serializer_result['request_start_datetime']
            request_end_datetime = ot_serializer_result['request_end_datetime']
            start_time = get_time_from_datetime(request_start_datetime)
            end_time = get_time_from_datetime(request_end_datetime)
            # ot_serializer_result['request_total_datetime'] = str(
            #     request_end_datetime - request_start_datetime)

            if not compare_date_and_datetime(request_start_datetime, ot_serializer_result['date']) \
                    or not compare_date_and_datetime(request_end_datetime, ot_serializer_result['date']):
                raise BadRequest(message="Date must be in one single day")
            print('pass 1')
            if ot_serializer_result['date'] < datetime.datetime.now().date() and ot_serializer_result['status'] == 0:
                raise BadRequest(message="Past Date Cant be request")
            print('pass 2')
            if is_it_off_day(ot_serializer_result['date']) or \
                    check_holiday(convert_date_to_str(ot_serializer_result['date'], False), False) is not None:
                result = create(ot_serializer_result, employee, request_emp)
                return ResponseUtil.success_response(result, create=True, model="Overtime")
            else:
                first_condition = False
                second_condition = False
                if working_start_time <= start_time <= working_end_time:
                    print('start time overlap')
                    first_condition = True
                if working_start_time <= end_time <= working_end_time:
                    print('end time overlap')
                    second_condition = True
                if first_condition or second_condition:
                    print('overlapped')
                    raise Exception('Overlapping working time,Please check ')
                result = create(ot_serializer_result, employee, request_emp)
                if(request_body.get('status') == 0):
                    message = "Dear "+request_emp.name+",\n\n\tI kindly request you to review my overtime request in In-House Attendance.\n\nSincerely,\n" + \
                        employee.name + "\n" + employee.department.name + " Department"
                    send_mail(subject="Overtime Request", message=message, from_email=employee.email, recipient_list=[request_emp.email],
                              fail_silently=False)
                return ResponseUtil.success_response(result, create=True, model="Overtime")
        else:
            raise BadRequest(message=overtime_serializer.errors)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['GET', 'POST'])
# @authentication_classes([TokenAuthentication])
# @permission_classes([IsAuthenticated])
def overtime_get_post(request):
    try:
        current_emp = get_user_from_token(request)
        if request.method == "GET":
            emp_id = request.GET.get("emp_id")
            request_emp_id = request.GET.get("request_emp_id")
            year = request.GET.get("year")
            if emp_id is not None and request_emp_id is None and year is not None:
                overtime_list = Overtime.objects.filter(
                    date__year=year, emp__id=emp_id)
            elif request_emp_id is not None and emp_id is None and year is not None:
                overtime_list = Overtime.objects.filter(
                    date__year=year, request_emp__id=request_emp_id)
            elif request_emp_id is not None and emp_id is not None and year is not None:
                overtime_list = Overtime.objects.filter(
                    date__year=year, request_emp__id=request_emp_id)
            else:
                raise BadRequest()
            result_list = OvertimeSerializer(
                overtime_list, many=True) if len(overtime_list) > 0 else None
            return ResponseUtil.success_response(result=result_list.data if result_list is not None else [], retrieve=True, model="Overtime")
        if request.method == "POST":
            request_body = JSONParser().parse(request)
            request_start_datetime = request_body['request_start_datetime']
            request_end_datetime = request_body['request_end_datetime']
            emp = Employee.objects.get(pk=request_body["emp"]["id"])
            request_emp = Employee.objects.get(
                pk=request_body["request_emp"]["id"])

            if timestamp_to_date(request_body['date']) < datetime.datetime.now().date() and request_body['status'] == 0:
                raise BadRequest(message="Past Date Cant be request")
            if request_body['status'] == 1 and current_emp.id != request_emp.id:
                raise BadRequest(
                    message="You can only create approved overtime reported to yourself")
            if timestamp_to_date(request_body['date']) < datetime.datetime.now().date() and request_body['status'] == 1 and current_emp.id == request_emp.id:
                request_body['actual_start_datetime'] = request_start_datetime
                request_body['actual_end_datetime'] = request_end_datetime

            overtime_serializer = OvertimeSerializer(data=request_body)
            if overtime_serializer.is_valid():
                result = create(
                    overtime_serializer.validated_data, emp, request_emp)
                if(request_body.get('status') == 0):
                    message = "Dear "+request_emp.name+",\n\n\tI kindly request you to review my overtime request in In-House Attendance.\n\nSincerely,\n" + \
                        emp.name + "\n" + emp.department.name + " Department"
                    send_mail(subject="Overtime Request", message=message, from_email=emp.email, recipient_list=[request_emp.email],
                              fail_silently=False)
                return ResponseUtil.success_response(result=result, create=True, model="Overtime")
            else:
                raise BadRequest(message=overtime_serializer.errors)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        print(e)
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['PUT'])
def overtime_check_in(request):
    try:
        current_emp = get_user_from_token(request)
        today_str_date = convert_date_to_str(
            datetime.datetime.now().date(), False)
        if check_overtime(today_str_date, current_emp) is None:
            raise BadRequest(message="No Overtime Request for Today")
        try:
            overtime = Overtime.objects.get(
                date=datetime.datetime.now().date(), emp=current_emp, del_flag=0)
        except Exception as e:
            raise NotFound(
                message="No Overtime Request for Today", error=str(e))
        if not is_it_off_day(datetime.datetime.now().date()) or check_holiday(today_str_date, False) is None:
            if working_end_time <= datetime.datetime.now().time() <= working_start_time:
                raise BadRequest(message="Working Time Overlap")
        if overtime.actual_start_datetime is not None:
            raise BadRequest(message="Already Check In")
        overtime.actual_start_datetime = datetime.datetime.now()
        overtime.updated_date = datetime.datetime.now()
        overtime.save()
        overtime_serializer = OvertimeSerializer(overtime)
        return ResponseUtil.success_response(result=overtime_serializer.data, update=True, model="Overtime")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['PUT'])
def overtime_check_out(request):
    try:
        current_emp = get_user_from_token(request)
        today_str_date = convert_date_to_str(
            datetime.datetime.now().date(), False)
        if check_overtime(today_str_date, current_emp) is None:
            raise BadRequest(message="No Overtime Request for Today")
        try:
            overtime = Overtime.objects.get(
                date=datetime.datetime.now().date(), emp=current_emp)
        except Exception as e:
            raise NotFound(
                message="No Overtime Request for Today", error=str(e))
        if overtime.actual_start_datetime is None:
            raise BadRequest(message="Cant Check out without even checked in")
        overtime.actual_end_datetime = datetime.datetime.now()
        # overtime.actual_total_datetime = str(
        #     overtime.actual_end_datetime - overtime.actual_start_datetime)
        overtime.updated_date = datetime.datetime.now()
        overtime.save()
        overtime_serializer = OvertimeSerializer(overtime)
        return ResponseUtil.success_response(result=overtime_serializer.data, update=True, model="Overtime")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        print(e.message)
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


def create(validated_data, emp, request_emp):
    overtime = Overtime(date=validated_data['date'],
                        request_start_datetime=validated_data['request_start_datetime'],
                        request_end_datetime=validated_data['request_end_datetime'],
                        status=validated_data['status'],
                        # request_total_datetime=validated_data['request_total_datetime'],
                        actual_start_datetime=validated_data['actual_start_datetime'] if 'actual_start_datetime' in
                                                                                         validated_data else None,
                        actual_end_datetime=validated_data['actual_end_datetime'] if 'actual_end_datetime' in
                                                                                     validated_data else None,
                        # actual_total_datetime=validated_data['actual_total_datetime'],
                        updated_id=validated_data['updated_id'] if 'updated_id' in validated_data else emp.id,
                        created_id=validated_data['created_id'] if 'created_id' in validated_data else emp.id,
                        del_flag=validated_data['del_flag'] if 'del_flag' in validated_data else 0,
                        emp=emp,
                        request_emp=request_emp)
    overtime.save()
    overtime_serializer = OvertimeSerializer(overtime)
    return overtime_serializer.data


@api_view(['PUT', 'DELETE'])
def overtime_modified(request, pk):
    try:
        overtime = Overtime.objects.get(pk=pk)
        if request.method == "PUT":
            request_body = JSONParser().parse(request)
            overtime_serializer = OvertimeSerializer(
                overtime, data=request_body)
            if overtime_serializer.is_valid():
                result = overtime_serializer.update(
                    overtime, overtime_serializer.validated_data)
                return ResponseUtil.success_response(result=result, update=True, model="Overtime")
            else:
                raise BadRequest(message=overtime_serializer.errors)
        if request.method == "DELETE":
            overtime.delete()
            return ResponseUtil.success_response(result=None, delete=True, model="Overtime")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST', 'GET'])
def test_func(request):
    result_timestamp = convert_datetime_to_timestamp(
        convert_str_to_datetime(JSONParser().parse(request)['datetime']))
    return JsonResponse(ApiResponse.successResponse(result={'timestamp': result_timestamp}))
