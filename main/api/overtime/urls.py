from django.urls import path
from main.api.overtime.views import *
urlpatterns = [
    path('', overtime_get_post),
    path('/request', requestOvertime),
    path('/check-in', overtime_check_in),
    path('/check-out', overtime_check_out),
    path('/<int:pk>', overtime_modified),
    path('/test', test_func)
]