import datetime

from rest_framework import serializers

from main.api.employee.models import Employee
from main.api.employee.serializers import EmployeeSerializer
from main.api.overtime.models import Overtime
from main.serializers import JsTimeStampFieldForDate, JsTimeStampFieldForDateTime


class OvertimeSerializer(serializers.ModelSerializer):
    emp = EmployeeSerializer(read_only=True)
    request_emp = EmployeeSerializer(read_only=True)
    date = JsTimeStampFieldForDate()
    request_start_datetime = JsTimeStampFieldForDateTime(
        allow_null=True, required=False)
    request_end_datetime = JsTimeStampFieldForDateTime(
        allow_null=True, required=False)
    actual_start_datetime = JsTimeStampFieldForDateTime(
        allow_null=True, required=False)
    actual_end_datetime = JsTimeStampFieldForDateTime(
        allow_null=True, required=False)
    created_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)
    updated_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)

    class Meta:
        model = Overtime
        fields = "__all__"

    def update(self, instance, validated_data):
        if self.initial_data.get('emp'):
            emp = Employee.objects.get(pk=self.initial_data['emp']['id'])
            instance.emp = emp
        if self.initial_data.get('request_emp'):
            request_emp = Employee.objects.get(
                pk=self.initial_data['request_emp']['id'])
            instance.request_emp = request_emp
        instance.status = validated_data.get('status', instance.status)
        instance.date = validated_data.get('date', instance.date)
        instance.request_start_datetime = validated_data.get(
            'request_start_datetime', instance.request_start_datetime)
        instance.request_end_datetime = validated_data.get(
            'request_end_datetime', instance.request_end_datetime)
        instance.actual_start_datetime = validated_data.get(
            'actual_start_datetime', instance.actual_start_datetime)
        instance.actual_end_datetime = validated_data.get(
            'actual_end_datetime', instance.actual_end_datetime)
        instance.updated_date = datetime.datetime.now()
        instance.save()
        result = OvertimeSerializer(instance)
        return result.data
