from django.db import models
import datetime
from main.api.employee.models import Employee


class Overtime(models.Model):
    date = models.DateField()

    request_start_datetime = models.DateTimeField()
    status = models.IntegerField(default=0)
    request_end_datetime = models.DateTimeField()
    actual_start_datetime = models.DateTimeField(null=True, blank=True)
    actual_end_datetime = models.DateTimeField(null=True, blank=True)
    del_flag = models.IntegerField(default=0)
    emp = models.ForeignKey(to=Employee, on_delete=models.CASCADE, related_name="requested_overtimes")
    request_emp = models.ForeignKey(to=Employee, on_delete=models.CASCADE, related_name="overtimes")
    created_id = models.IntegerField(default=0)
    updated_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    updated_date = models.DateTimeField(default=datetime.datetime.now)


    class Meta:
        db_table = "tbl_overtime"
