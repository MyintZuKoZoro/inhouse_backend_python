from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from main.api.department.models import Department
from main.api.department.serializers import DepartmentSerializer
from rest_framework import status

from main.api.employee.models import Employee
from main.response import ApiResponse


@api_view(['GET', 'POST'])
def department_list(request):
    if request.method == 'GET':
        department_list = Department.objects.filter(del_flag = 0)
        department_serializer = DepartmentSerializer(department_list, many=True)

        return JsonResponse(ApiResponse.formatResponse(result=department_serializer.data,
                                                       message="Retrieved Department Successfully",
                                                       success=True,
                                                       code=status.HTTP_200_OK,
                                                       error=None), safe=False)
    elif request.method == 'POST':
        department_data = JSONParser().parse(request)
        department_serializer = DepartmentSerializer(data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse(ApiResponse.formatResponse(result=department_serializer.data,
                                                           message="Created Department Successfully",
                                                           success=True,
                                                           code=status.HTTP_201_CREATED,
                                                           error=None))
        return JsonResponse(ApiResponse.formatResponse(result=None,
                                                       message="Errors in Creating Department",
                                                       success=False,
                                                       code=status.HTTP_400_BAD_REQUEST,
                                                       error=department_serializer.errors))


@api_view(['PUT', 'POST'])
def department_modified(request, pk):
    try:
        department = Department.objects.get(pk=pk)
    except department.DoesNotExist:
        return JsonResponse(ApiResponse.formatResponse(result=None,
                                                       message="Department does not exit",
                                                       success=False,
                                                       code=status.HTTP_404_NOT_FOUND,
                                                       error=None))



    if request.method == 'PUT':
        department_data = JSONParser().parse(request)
        department_serializer = DepartmentSerializer(department, data=department_data)
        if department_serializer.is_valid():
            department_serializer.save()
            return JsonResponse(ApiResponse.formatResponse(result=department_serializer.data,
                                                           message="Updated Department Successfully",
                                                           success=True,
                                                           code=status.HTTP_200_OK,
                                                           error=None))
        return JsonResponse(ApiResponse.formatResponse(result=department_serializer.data,
                                                       message="Errors in Updating Department",
                                                       success=True,
                                                       code=status.HTTP_200_OK,
                                                       error=department_serializer.errors))

    elif request.method == 'POST':
        replace_department_parse = JSONParser().parse(request)

        try:
            replace_department = Department.objects.get(pk=replace_department_parse['replace_department_id'])
        except replace_department.DoesNotExist:
            return JsonResponse(ApiResponse.formatResponse(result=None,
                                                           message="Replaced Department does not exit",
                                                           success=False,
                                                           code=status.HTTP_404_NOT_FOUND,
                                                           error=None))
        department.del_flag = 1
        department.save()
        Employee.objects.filter(department=department).update(department=replace_department)
        return JsonResponse(ApiResponse.formatResponse(result=None,
                                                       message="Deleted Department Successfully",
                                                       success=True,
                                                       code=status.HTTP_200_OK,
                                                       error=None))
