from django.db import models
from datetime import datetime


class Department(models.Model):
    name = models.CharField(max_length=100)
    created_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.now)
    del_flag = models.IntegerField(default=0)

    class Meta:
        db_table = "tbl_department"

    def __str__(self):
        return self.name
