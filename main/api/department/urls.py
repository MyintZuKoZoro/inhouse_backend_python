from django.urls import path
from main.api.department import views
from django.conf.urls import url

urlpatterns = [
    path('', views.department_list, name='department_list'),
    path('/<int:pk>', views.department_modified , name='department_modified'),

]
