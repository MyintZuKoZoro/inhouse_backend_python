from django.urls import path
from main.api.employee import views

urlpatterns = [
    path('', views.employee_list),
    path('/<int:pk>', views.employee_modified, name='employee_modified'),
    path('/<int:pk>/upload-image', views.upload_image, name='image'),
    path('/change-password', views.changePassword),
]