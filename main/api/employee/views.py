from django.forms import model_to_dict
from django.http import JsonResponse
from django.db.models import Q
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, parser_classes
from rest_framework.parsers import JSONParser
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response

from main.util.ExceptionUtil import InternalServerError, BadRequest, NotFound
from main.util.ResponseUtil import ResponseUtil
from authentication.models import User
from authentication.serializers import UserSerializer
from main.api.department.models import Department
from main.api.department.serializers import DepartmentSerializer
from main.api.employee.models import Employee
from main.api.employee.serializers import EmployeeSerializer
from main.response import ApiResponse
from main.util.CommonUtil import get_user_from_token


@api_view(['GET', 'POST'])
def employee_list(request):
    if request.method == 'GET':
        print(request.GET)
        keyword = request.GET.get('keyword')
        department_id = int(request.GET.get('department_id')) if request.GET.get(
            'department_id') is not None else None
        if keyword is not None and keyword != '' and department_id is not None and department_id != '':
            employee_filter = Employee.objects.filter(Q(name__icontains=keyword) | Q(code__icontains=keyword),
                                                      department__id=department_id, del_flag=0)
            employee_serializer = EmployeeSerializer(
                employee_filter, many=True)
            return ResponseUtil.success_response(result=employee_serializer.data, retrieve=True, model="Employee")

        elif keyword is not None and keyword != '':
            employee_filter = Employee.objects.filter(Q(name__icontains=keyword) | Q(code__icontains=keyword),
                                                      del_flag=0)
            employee_serializer = EmployeeSerializer(
                employee_filter, many=True)
            return ResponseUtil.success_response(result=employee_serializer.data, retrieve=True, model="Employee")

        elif department_id is not None and department_id != '':
            employee_filter = Employee.objects.filter(
                department__id=department_id, del_flag=0)
            employee_serializer = EmployeeSerializer(
                employee_filter, many=True)
            return ResponseUtil.success_response(result=employee_serializer.data, retrieve=True, model="Employee")

        else:
            employee_list = Employee.objects.filter(del_flag=0)
            employee_serializer = EmployeeSerializer(employee_list, many=True)
            return ResponseUtil.success_response(result=employee_serializer.data, retrieve=True, model="Employee")

    elif request.method == 'POST':
        employee_data = JSONParser().parse(request)
        print('registering employee')
        password = "Exe123"
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            try:
                employee = employee_serializer.save()
            except Exception as e:
                return ResponseUtil.error_response(InternalServerError(error=str(e)))
            temp_user = {"emp_id": employee.id, "email": employee.email, "password": password,
                         "username": employee.username}
            token = UserSerializer.create_user_and_get_token(
                self=User, validated_data=temp_user)
            print(token)
            return ResponseUtil.success_response(result=employee_serializer.data, create=True, model="Employee")

        print('Not Valid')
        return ResponseUtil.error_response(error_type=400, error=employee_serializer.errors)


@api_view(['PUT'])
@parser_classes([MultiPartParser])
def upload_image(request, pk):
    try:
        employee = Employee.objects.get(pk=pk)
    except employee.DoesNotExist:
        return ResponseUtil.error_response(error_type=404)

    employee.image_url = request.data.get('image')
    employee_serializer = EmployeeSerializer(
        employee, data=employee.__dict__, context={'request': request})

    if employee_serializer.is_valid():
        employee_serializer.save()
        return JsonResponse(ApiResponse.formatResponse(result=employee_serializer.data,
                                                       message="Uploaded Image Successfully",
                                                       success=True,
                                                       code=status.HTTP_200_OK,
                                                       error=None))
    else:
        return JsonResponse(ApiResponse.formatResponse(result=None,
                                                       message="Errors in Uploading Image",
                                                       success=False,
                                                       code=status.HTTP_400_BAD_REQUEST,
                                                       error=employee_serializer.errors))


@api_view(['PUT', 'POST', 'GET'])
def employee_modified(request, pk):
    try:
        employee = Employee()
        try:
            employee = Employee.objects.get(pk=pk)
        except employee.DoesNotExist:
            return ResponseUtil.error_response(error_type=404)

        if request.method == 'GET':
            employee_serializer = EmployeeSerializer(employee)
            return ResponseUtil.success_response(result=employee_serializer.data, retrieve=True, model="Employee")

        if request.method == 'PUT':
            employee_data = JSONParser().parse(request)
            current_emp = get_user_from_token(request)

            if current_emp.role == 2 and current_emp.id == employee_data.get('id'):
                dept = model_to_dict(employee.department)
                role2_emp = model_to_dict(employee)
                role2_emp['department'] = dept
                role2_emp['username'] = employee_data.get(
                    'username', employee.username)
                role2_emp['phone'] = employee_data.get('phone', employee.phone)
                role2_emp['address'] = employee_data.get(
                    'address', employee.address)

                employee_serializer = EmployeeSerializer(
                    employee, data=role2_emp)
                if employee_serializer.is_valid():
                    employee_serializer.save()
                    return ResponseUtil.success_response(result=employee_serializer.data, update=True, model="Employee")

            else:
                employee_serializer = EmployeeSerializer(
                    employee, data=employee_data)
                if employee_serializer.is_valid():
                    employee_serializer.save()
                    return ResponseUtil.success_response(result=employee_serializer.data, update=True, model="Employee")

            return ResponseUtil.error_response(error_type=400, error=employee_serializer.errors)

        elif request.method == 'POST':
            employee.del_flag = 1
            employee.save()
            return ResponseUtil.success_response(result={}, delete=True, model="Employee")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST'])
def changePassword(request):
    try:
        request_data = JSONParser().parse(request)
        token = request.headers["Authorization"][6:]
        token_object = Token.objects.get(key=token)
        try:
            user = User.objects.get(pk=token_object.user_id)
        except Exception as e:
            raise NotFound(message=str(e))
        if user.check_password(request_data['old_password']):
            user.set_password(request_data['new_password'])
            user.save()
            return ResponseUtil.message_response(None, "Password Successfully Changed")
        else:
            raise BadRequest(message="Wrong Old Password")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))
