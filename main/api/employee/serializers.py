from authentication.models import User
from rest_framework import serializers

from main.api.department.models import Department
from main.api.department.serializers import DepartmentSerializer
from main.api.employee.models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    department = DepartmentSerializer(read_only=True, many=False)
    image_url = serializers.ImageField(
        max_length=None, use_url=True, allow_null=True, required=False)

    class Meta:
        model = Employee
        fields = ('id',
                  'name',
                  'code',
                  'username',
                  'email',
                  'phone',
                  'image_url',
                  'address',
                  'position',
                  'role',
                  'department',
                  'joined_date',
                  'del_flag',
                  'created_id',
                  'created_date',
                  'updated_id',
                  'updated_date')

    def create(self, validated_data):
        department = None
        if self.initial_data.get('department'):
            department = Department.objects.get(
                pk=self.initial_data['department']['id'])
        employee = Employee.objects.create(
            **validated_data, department=department)
        return employee

    def update(self, instance, validated_data):
        user = User.objects.get(emp__id=instance.id)
        isUserUpdated = False
        if self.initial_data.get('department'):
            department = Department.objects.get(
                pk=self.initial_data['department']['id'])
            instance.department = department
        instance.name = validated_data.get('name', instance.name)
        instance.code = validated_data.get('code', instance.code)
        if validated_data.get('username') is not None and validated_data.get('username') != instance.username:
            user.username = validated_data.get('username')
            isUserUpdated = True
        instance.username = validated_data.get('username', instance.username)
        if validated_data.get('email') is not None and validated_data.get('email') != instance.email:
            user.email = validated_data.get('email')
            isUserUpdated = True
        instance.email = validated_data.get('email', instance.email)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.address = validated_data.get('address', instance.address)
        instance.position = validated_data.get('position', instance.position)
        instance.role = validated_data.get('role', instance.role)
        instance.joined_date = validated_data.get(
            'joined_date', instance.joined_date)
        instance.del_flag = validated_data.get('del_flag', instance.del_flag)
        instance.created_date = validated_data.get(
            'created_date', instance.created_date)
        instance.created_id = validated_data.get(
            'created_id', instance.created_id)
        instance.updated_date = validated_data.get(
            'updated_date', instance.updated_date)
        instance.updated_id = validated_data.get(
            'updated_id', instance.updated_id)
        instance.save()
        if isUserUpdated:
            user.save()
        return instance

    def get_image_url(self, obj):
        request = self.context.get("request")
        return request.build_absolute_uri(obj.image.url)
