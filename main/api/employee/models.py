from django.db import models
from main.api.department.models import Department
from datetime import datetime


def upload_to(instance, filename):
    return '/'.join(['images', filename])


class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)
    username = models.CharField(max_length=100, unique=True)
    email = models.EmailField(max_length=100, unique=True)
    phone = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    role = models.IntegerField(default=0)
    joined_date = models.DateField()
    created_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.now)
    del_flag = models.IntegerField(default=0)
    image_url = models.ImageField(upload_to=upload_to, null=True, blank=True)

    class Meta:
        db_table = "tbl_employee"

    # def __str__(self):
    #     return self.name
