from django.urls import path
from main.api.leave_allowance import views

urlpatterns = [
    path('', views.leave_allowance),
    path('/<int:pk>', views.leave_leave_allowance_modified)
]
