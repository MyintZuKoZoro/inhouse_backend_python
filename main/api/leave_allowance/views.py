import datetime

from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated

from main.api.employee.models import Employee
from main.api.leave_type.models import LeaveType
from main.api.leave_allowance.models import LeaveAllowance
from main.api.leave_allowance.serializers import LeaveAllowanceSerializer
from main.util.CommonUtil import get_user_from_token
from main.util.ExceptionUtil import BadRequest, InternalServerError, NotFound
from main.util.ResponseUtil import ResponseUtil


@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def leave_allowance(request):
    try:
        if request.method == 'GET':
            request_emp_id = request.GET.get('emp_id')
            request_year = request.GET.get('year')
            if request_emp_id is not None and request_year is not None:
                leave_allowance_lists = LeaveAllowance.objects.filter(employee__id=(request_emp_id), created_date__year=request_year,
                                                                      del_flag=0)
            else:
                leave_allowance_lists = LeaveAllowance.objects.filter(
                    del_flag=0)

            leave_allowance_serializer = LeaveAllowanceSerializer(
                leave_allowance_lists, many=True)
            return ResponseUtil.success_response(result=leave_allowance_serializer.data, retrieve=True,
                                                 model='Leave Allowance')
        elif request.method == 'POST':
            leave_allowance_data = JSONParser().parse(request)
            # current user
            current_user = get_user_from_token(request)
            if not ('created_id' in leave_allowance_data):
                leave_allowance_data['created_id'] = current_user.id
            if not ('updated_id' in leave_allowance_data):
                leave_allowance_data['updated_id'] = current_user.id

            leave_allowance_serializer = LeaveAllowanceSerializer(
                data=leave_allowance_data)
            if leave_allowance_serializer.is_valid():
                leave_allowance_serializer.save()
                return ResponseUtil.success_response(result=leave_allowance_serializer.data, create=True,
                                                     model='Leave Allowance')
            else:
                raise BadRequest()

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        print(error)
        return ResponseUtil.error_response(InternalServerError(error=str(error)))


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def leave_leave_allowance_modified(request, pk):
    try:
        try:
            current_user = get_user_from_token(request)
            leave_allowance_list = LeaveAllowance.objects.get(
                id=pk, del_flag=0)
        except Exception as error:
            raise NotFound(error=str(error))

        if request.method == 'GET':
            leave_allowance_serializer = LeaveAllowanceSerializer(
                leave_allowance_list)
            return ResponseUtil.success_response(result=leave_allowance_serializer.data, retrieve=True,
                                                 model='Leave Allowance')

        elif request.method == 'PUT':
            leave_allowance_data = JSONParser().parse(request)
            employee = Employee.objects.get(
                pk=leave_allowance_data['employee']['id'])
            leave_type = LeaveType.objects.get(
                pk=leave_allowance_data['leave_type']['id'])
            leave_allowance_data['employee'] = employee
            leave_allowance_data['leave_type'] = leave_type
            if not ('updated_id' in leave_allowance_data):
                leave_allowance_data['updated_id'] = current_user.id
            leave_allowance_serializer = LeaveAllowanceSerializer(
                leave_allowance_list, data=leave_allowance_data)
            if leave_allowance_serializer.is_valid():
                leave_allowance_serializer.update(
                    leave_allowance_list, leave_allowance_data)
                return ResponseUtil.success_response(result=leave_allowance_serializer.data, update=True,
                                                     model='Leave Allowance')
            else:
                raise BadRequest()

        elif request.method == 'POST':
            leave_allowance_list.del_flag = 1
            leave_allowance_list.updated_id = current_user.id
            leave_allowance_list.save()
            return ResponseUtil.success_response(result=LeaveAllowanceSerializer(leave_allowance_list).data, delete=True,
                                                 model='Leave Allowance')

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))


def crud_leave_allowance_by_leave_type(request, temp_leave_type, op_type):
    try:
        # current user
        current_user = get_user_from_token(request)
        if op_type == 'CREATE':
            try:
                employee_lists = Employee.objects.filter(del_flag=0)
            except Exception as e:
                raise NotFound(error=str(e))

            for employee in employee_lists:
                exp_months = (
                    (datetime.date.today() - employee.joined_date) / 30).days
                if int(exp_months) >= int(temp_leave_type['valid_at']):
                    leave_allowance_data = LeaveAllowance(
                        del_flag=0,
                        created_id=current_user.id,
                        updated_id=current_user.id,
                        employee_id=employee.id,
                        leave_type_id=temp_leave_type['id']
                    )
                    leave_allowance_data.save()
                else:
                    # exp_months < valid_at
                    # TODO
                    raise Exception(
                        "Employee is not allowance for this LeaveType")
            return ResponseUtil.success_response(result=[], create=True, model='Leave Allowance')

        else:
            try:
                temp_leave_allowance_lists = LeaveAllowance.objects.filter(leave_type_id=temp_leave_type['id'],
                                                                           del_flag=0)
            except Exception as e:
                raise NotFound(error=str(e))

            for temp_leave_allowance_list in temp_leave_allowance_lists:
                if op_type == 'UPDATE':
                    leave_allowance_data = {
                        'del_flag': temp_leave_allowance_list.del_flag,
                        'created_id': temp_leave_allowance_list.created_id,
                        'updated_id': current_user.id,
                        'employee': temp_leave_allowance_list.employee,
                        'leave_type': temp_leave_allowance_list.leave_type_id
                    }
                elif op_type == 'DELETE':
                    leave_allowance_data = {
                        'del_flag': 1,
                        'updated_id': current_user.id,
                        'leave_type': temp_leave_allowance_list.leave_type_id
                    }
                leave_allowance_serializer = LeaveAllowanceSerializer(temp_leave_allowance_list,
                                                                      data=leave_allowance_data)
                if leave_allowance_serializer.is_valid():
                    leave_allowance_serializer.update(temp_leave_allowance_list,
                                                      leave_allowance_serializer.validated_data)

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))
