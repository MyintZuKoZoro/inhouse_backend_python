from main.api.leave_type.models import LeaveType
from main.api.employee.models import Employee
import django_filters
from rest_framework import serializers

from main.api.employee.serializers import EmployeeSerializer
from main.api.leave_allowance.models import LeaveAllowance
from main.api.leave_type.serializers import LeaveTypeSerializer


class LeaveAllowanceSerializer(serializers.ModelSerializer):
    employee = EmployeeSerializer(read_only=True)
    leave_type = LeaveTypeSerializer(read_only=True)

    class Meta:
        model = LeaveAllowance
        fields = '__all__'

    def create(self, validated_data):
        employee = None
        leave_type = None
        if self.initial_data.get('employee'):
            employee = Employee.objects.get(
                pk=self.initial_data.get('employee')['id'])
        if self.initial_data.get('leave_type'):
            leave_type = LeaveType.objects.get(
                pk=self.initial_data.get('leave_type')['id'])

        leave = LeaveAllowance.objects.create(
            **validated_data, employee=employee, leave_type=leave_type)
        return leave
