from django.db import models
from datetime import datetime

from main.api.employee.models import Employee
from main.api.leave_type.models import LeaveType


class LeaveAllowance(models.Model):
    employee = models.ForeignKey(
        Employee, on_delete=models.CASCADE, related_name='allowance_employee_id')
    leave_type = models.ForeignKey(
        LeaveType, on_delete=models.CASCADE, related_name='allowance_leave_type_id')
    # leave_type_name = models.CharField(max_length=50)
    # valid_days = models.FloatField(default=0)
    used_days = models.FloatField(default=0)
    del_flag = models.IntegerField(default=0)
    created_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "tbl_leave_allowance"
