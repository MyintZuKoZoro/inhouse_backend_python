from rest_framework import serializers

from main.api.attendance.models import Attendance
import datetime
from main.api.employee.serializers import EmployeeSerializer
from main.serializers import JsTimeStampFieldForDate, JsTimeStampFieldForDateTime


class AttendanceSerializer(serializers.ModelSerializer):
    emp = EmployeeSerializer(read_only=True)
    date = JsTimeStampFieldForDate()
    check_in_time = JsTimeStampFieldForDateTime()
    check_out_time = JsTimeStampFieldForDateTime(
        allow_null=True, required=False)
    created_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)
    updated_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)

    class Meta:
        model = Attendance
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.check_in_time = validated_data.get(
            'check_in_time', instance.check_in_time)
        instance.check_out_time = validated_data.get(
            'check_out_time')
        instance.updated_date = datetime.datetime.now()
        instance.save()
        return instance
