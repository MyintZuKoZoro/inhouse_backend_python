from django.db import models

from main.api.employee.models import Employee
import datetime


class Attendance(models.Model):
    check_in_time = models.DateTimeField(null=True, blank=True)
    check_out_time = models.DateTimeField(null=True, blank=True)
    date = models.DateField()
    emp = models.ForeignKey(to=Employee, related_name="attendances", on_delete=models.CASCADE)
    created_id = models.IntegerField(default=0)
    updated_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    updated_date = models.DateTimeField( default=datetime.datetime.now)
    del_flag = models.IntegerField(default=0)

    class Meta:
        db_table = "tbl_attendance"
