import datetime
from main.api.employee.serializers import EmployeeSerializer
from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from rest_framework.response import Response

from main.api.attendance.models import Attendance
from main.api.attendance.serializers import AttendanceSerializer
from main.api.employee.models import Employee
from main.response import ApiResponse

from main.util.CommonUtil import check_holiday, check_overtime, check_attendance, \
    is_it_off_day, convert_date_to_str, get_user_from_token, compare_timestamp_and_date, \
    convert_str_to_date, convert_date_to_timestamp, compare_date_and_datetime, check_leave
from main.util.ExceptionUtil import UnauthorizedException, NotFound, InternalServerError, BadRequest
from calendar import monthrange

from main.util.ResponseUtil import ResponseUtil


@api_view(['POST', 'GET'])
def attendance_post_get(request):
    try:
        if request.method == "POST":
            current_user = get_user_from_token(request)
            request_body = JSONParser().parse(request)
            try:
                emp = Employee.objects.get(pk=request_body["emp"]["id"])
            except Exception as e:
                raise NotFound(error=str(e))
            attendance_serializer = AttendanceSerializer(data=request_body)
            if attendance_serializer.is_valid():
                date = attendance_serializer.validated_data['date']
                check_duplicate = Attendance.objects.filter(
                    emp=emp, date=date, del_flag=0)
                if len(check_duplicate) > 0:
                    raise BadRequest(message="Already Applied Date")
                # if date < datetime.datetime.now().date():
                #     raise Exception('Past Date Cant Be applied')
                if is_it_off_day(date) or check_holiday(
                        convert_date_to_str(date, False), False) is not None:
                    raise BadRequest(message="Its Work Off Day")
                if check_leave(date, emp) is not None:
                    raise BadRequest(message="Its leave day of that employee")
                if attendance_serializer.validated_data['check_in_time'] is None:
                    raise BadRequest(
                        message='Checkin time is necessary for new attendance')
                if not compare_date_and_datetime(attendance_serializer.validated_data['check_in_time'], date):
                    raise BadRequest(
                        message="Checkin time must be in attenance date")
                if 'check_out_time' in attendance_serializer.validated_data is not None:
                    if not compare_date_and_datetime(attendance_serializer.validated_data['check_out_time'], date):
                        raise BadRequest(
                            message="Checkout time must be in attenance date")

                result = create(
                    attendance_serializer.validated_data, emp, current_user)
                return ResponseUtil.success_response(result=result, create=True, model="Attendance")
            else:
                raise BadRequest(message=attendance_serializer.errors)

        if request.method == "GET":
            year = request.GET.get("year")
            param_date = request.GET.get("date")
            emp_id = request.GET.get("emp_id")
            if param_date is not None:
                employee_list = Employee.objects.filter(del_flag=0)
                result_list = []
                for employee in employee_list:
                    that_date = param_date
                    holiday = check_holiday(that_date, False)
                    overtime = check_overtime(that_date, employee)
                    attendance = check_attendance(that_date, employee)
                    leave = check_leave(that_date, employee)
                    that_date = convert_date_to_timestamp(that_date)
                    result_list.append(attendance_result_list(
                        overtime, holiday, attendance, leave, that_date, EmployeeSerializer(employee).data))
                return ResponseUtil.success_response(result=result_list, retrieve=True, model="Attendance")
            if year is None or emp_id is None or request.GET.get("month") is None:
                raise BadRequest(message="Not Enough Request Param")
            month = "0" + str(int(request.GET.get("month")) + 1) if int(request.GET.get("month")) < 9 else str(
                int(request.GET.get("month")) + 1)
            employee = Employee.objects.get(pk=int(emp_id))
            result_list = []
            for i in range(monthrange(int(year), int(month))[-1]):
                i = i + 1
                that_date = str(year) + "-" + month + "-" + \
                    (str(i) if len(str(i)) > 1 else "0" + str(i))
                holiday = check_holiday(that_date, False)
                overtime = check_overtime(that_date, employee)
                attendance = check_attendance(that_date, employee)
                leave = check_leave(that_date, employee)
                that_date = convert_date_to_timestamp(that_date)
                result_list.append(attendance_result_list(
                    overtime, holiday, attendance, leave, that_date))

            return ResponseUtil.success_response(result=result_list, retrieve=True, model="Attendance")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST'])
def attendance_check_in(request):
    try:
        # to do to check emp leave day
        emp = get_user_from_token(request)
        if is_it_off_day(datetime.datetime.now()) or check_holiday(
                convert_date_to_str(datetime.datetime.now())) is not None:
            raise BadRequest(message="Its Work Off Day")
        if check_leave(datetime.datetime.now().date(), emp) is not None:
            raise BadRequest(message="Today is your leave day")
        attendance = Attendance(check_in_time=datetime.datetime.now(), check_out_time=None, emp=emp,
                                created_id=emp.id, updated_id=emp.id,
                                date=datetime.datetime.now())
        check_duplicate = Attendance.objects.filter(
            emp=emp, date=datetime.datetime.now().date(), del_flag=0)
        if len(check_duplicate) > 0:
            raise BadRequest(
                message="Employee with this date is already Checked In")
        attendance.save()
        attendance_serializer = AttendanceSerializer(attendance)
        return ResponseUtil.success_response(result=attendance_serializer.data, create=True, model="attendance")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['PUT'])
def attendance_check_out(request):
    try:
        emp = get_user_from_token(request)
        try:
            attendance = Attendance.objects.get(
                emp=emp, date=datetime.datetime.now().date(), del_flag=0)
        except Exception as e:
            raise BadRequest(
                message="Cant Check Out Without Check In", error=str(e))
        attendance.check_out_time = datetime.datetime.now()
        attendance.updated_date = datetime.datetime.now()
        attendance.save()
        attendance_serializer = AttendanceSerializer(attendance)
        return ResponseUtil.success_response(result=attendance_serializer.data, update=True, model="Attendance")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['PUT', 'DELETE'])
def attendance_modified(request, pk):
    try:
        current_user = get_user_from_token(request)
        try:
            attendance = Attendance.objects.get(pk=pk)
        except Exception as e:
            raise NotFound(error=str(e))
        if request.method == "PUT":
            # check_duplicate = Attendance.objects.filter(emp=attendance.emp, date=JSONParser().parse(request)["date"])
            # if len(check_duplicate) > 0:
            #     raise Exception('Employee with this Date is already applied')
            attendance_serializer = AttendanceSerializer(
                attendance, data=JSONParser().parse(request))
            if attendance_serializer.is_valid():
                attendance_serializer.update(
                    attendance, attendance_serializer.validated_data)
                return ResponseUtil.success_response(result=attendance_serializer.data, update=True, model="Attendance")
            else:
                raise BadRequest(message=attendance_serializer.errors)
        if request.method == "DELETE":
            # attendance.delete()
            attendance.del_flag = 1
            attendance.updated_id = current_user.id
            attendance.updated_date = datetime.datetime.now()
            attendance.save()
            return ResponseUtil.success_response(result=None, delete=True, model="Attendance")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


def create(validated_data, emp, current_user):
    attendance = Attendance(check_in_time=validated_data['check_in_time'],
                            check_out_time=validated_data['check_out_time'] if 'check_out_time' in validated_data else None,
                            date=validated_data['date'],
                            created_id=validated_data['created_id'] if 'created_id' in validated_data
                            else current_user.id,
                            updated_id=validated_data['updated_id'] if 'updated_id' in validated_data
                            else current_user.id,
                            emp=emp)
    attendance.save()
    attendance_serializer = AttendanceSerializer(attendance)
    return attendance_serializer.data


def attendance_result_list(overtime, holiday, attendance, leave, date, employee=None):
    result = {"overtime": overtime, "holiday": holiday,
              "attendance": attendance, "leave": leave, "date": date, "employee": employee}
    return result
