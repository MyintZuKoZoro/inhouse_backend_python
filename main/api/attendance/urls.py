from django.urls import path
from main.api.attendance.views import *

urlpatterns = [
    path('', attendance_post_get),
    path('/check-in', attendance_check_in),
    path('/check-out', attendance_check_out),
    path('/<int:pk>', attendance_modified)

]