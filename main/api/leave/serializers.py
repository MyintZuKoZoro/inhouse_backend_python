from rest_framework import serializers

from main.api.employee.models import Employee
from main.api.employee.serializers import EmployeeSerializer
from main.api.leave.models import Leave
from main.api.leave_type.models import LeaveType
from main.api.leave_type.serializers import LeaveTypeSerializer
from main.serializers import JsTimeStampFieldForDate


class LeaveSerializer(serializers.ModelSerializer):
    employee = EmployeeSerializer(read_only=True)
    supervisor = EmployeeSerializer(read_only=True)
    leave_type = LeaveTypeSerializer(read_only=True)
    leave_date = JsTimeStampFieldForDate()

    class Meta:
        model = Leave
        fields = '__all__'

    def create(self, validated_data):
        employee = None
        supervisor = None
        leave_type = None
        if self.initial_data.get('employee'):
            employee = Employee.objects.get(pk=self.initial_data.get('employee').id)

        if self.initial_data.get('supervisor'):
            supervisor = Employee.objects.get(pk=self.initial_data.get('supervisor').id)
        if self.initial_data.get('leave_type'):
            leave_type = LeaveType.objects.get(pk=self.initial_data.get('leave_type').id)

        leave = Leave.objects.create(**validated_data, employee=employee, supervisor=supervisor, leave_type=leave_type)
        return leave
