from django.db.models import Q
from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from datetime import datetime

from main.api.leave_allowance.models import LeaveAllowance
from main.util.ExceptionUtil import *
import datetime
import calendar
from main.api.department.models import Department
from main.api.leave.models import Leave
from main.api.leave.serializers import LeaveSerializer
from main.api.leave_type.models import LeaveType
from main.response import ApiResponse
from main.util.CommonUtil import *
from main.util.ResponseUtil import ResponseUtil
from django.core.mail import send_mail


@api_view(['GET', 'POST'])
def leave_request(request):
    try:
        if request.method == 'POST':
            request_data = JSONParser().parse(request)
            # to_date = timestamp_to_date(request_data.get('to_date'))
            # millisecond = datetime.datetime.now().date()
            # print(millisecond)
            # print(convert_date_to_timestamp(
            #         convert_str_to_date(request_data.get('from_date')),is_str_date=False))
            # print(convert_date_to_timestamp(
            #     convert_str_to_date(request_data.get('to_date')),is_str_date=False))

            # convert_all_timestamp_to_datetime_in_object(request_data, ("from_date", "date"),
            #                                             ("to_date", "date"))
            # print("{} mzk".format(request_data.get('from_date')))
            current_emp = get_user_from_token(request)
            if current_emp is None:
                raise UnauthorizedException()
            try:
                supervisor = Employee.objects.get(
                    pk=request_data.get('supervisor_id'), del_flag=0)
            except Exception as e:
                raise NotFound(
                    error=str(e), message="Requested Supervisor does not exit")
            try:
                leave_type = LeaveType.objects.get(
                    pk=request_data.get('leave_type_id'), del_flag=0)
            except Exception as e:
                raise NotFound(
                    error=str(e), message="Requested Leave type does not exit")

            employee = None
            if request_data.get('employee_id') is not None:
                employee = Employee.objects.get(
                    pk=request_data.get('employee_id'))

            print(request_data)

            print(request_data)
            if (timestamp_to_date(request_data['from_date']) < datetime.datetime.now().date() or
                    ((timestamp_to_date(request_data['to_date']) < datetime.datetime.now().date()) if request_data.get('to_date') is not None else False)) and request_data['status'] == 0:
                raise BadRequest(message="Past Date Cant be requested")
            if request_data['status'] == 1 and current_emp.id != supervisor.id:
                raise BadRequest(
                    message="You can only create approved leave reported to yourself")

            # For Leave only one day
            if request_data.get('to_date') is None:
                from_date = timestamp_to_date(request_data.get('from_date'))
                # Check if user requested leave in holiday
                holiday = check_holiday(from_date, True)
                if holiday is not None:
                    raise BadRequest(message="You cannot request for holiday")
                # Check if user requested leave in weekend
                weekend = is_it_off_day(from_date)
                if weekend:
                    raise BadRequest(message="You cannot request for weekend")
                # Check if user requested leave in already exiting leaves
                # Check exiting leaves with status pending and cancel
                leave_taken = Leave.objects.filter(Q(status=0) | Q(status=1),
                                                   leave_date=from_date, del_flag=0, employee=current_emp if employee is None else employee)
                if leave_taken.exists():
                    raise BadRequest(
                        message="You cannot request for already taken leave")
                print(employee.name)
                leave_data = {
                    'employee': current_emp if employee is None else employee,
                    'leave_type': leave_type,
                    'leave_date': request_data.get('from_date'),
                    'supervisor': supervisor,
                    'status': request_data['status'],
                    'del_flag': 0,
                    'type': request_data.get('from_type')

                }
                leave_serializer = LeaveSerializer(data=leave_data)
                if leave_serializer.is_valid():
                    leave = leave_serializer.save()
                    if(leave_data.get('status') == 0):
                        message = "Dear "+supervisor.name+",\n\n\tI kindly request you to review my leave request in In-House Attendance.\n\nSincerely,\n" + \
                            (current_emp if employee is None else employee).name + "\n" + (
                                current_emp if employee is None else employee).department.name + " Department"
                        send_mail(subject="Leave Request", message=message, from_email=(current_emp if employee is None else employee).email, recipient_list=[supervisor.email],
                                  fail_silently=False)
                    return ResponseUtil.success_response(result=leave_serializer.data, create=True, model="Leave")
                raise BadRequest()

            # For Leaves more than one day
            else:
                from_date = timestamp_to_date(request_data.get('from_date'))
                to_date = timestamp_to_date(request_data.get('to_date'))
                from_type = request_data.get('from_type')
                to_type = request_data.get('to_type')
                delta = datetime.timedelta(days=1)  # for day iterate
                leave_result = []
                i = 1

                # substract from date until it's working day
                is_from_date_valid = False
                while is_from_date_valid is not True:
                    is_holiday = check_holiday(from_date, True)
                    if is_holiday is not None:
                        from_type = 0
                        from_date += delta
                        continue

                    is_weekend = is_it_off_day(from_date)
                    if is_weekend:
                        from_type = 0
                        from_date += delta
                        continue

                    is_from_date_valid = True

                # substract to date until it's working day
                is_to_date_valid = False
                while is_to_date_valid is not True:
                    is_holiday = check_holiday(to_date, True)
                    if is_holiday is not None:
                        to_type = 0
                        to_date -= delta
                        continue

                    is_weekend = is_it_off_day(to_date)
                    if is_weekend:
                        to_type = 0
                        to_date -= delta
                        continue

                    is_to_date_valid = True

                daycount = (to_date - from_date).days + 1
                datecount = daycount
                if from_type != 0:
                    datecount -= 0.5
                if to_type != 0:
                    datecount -= 0.5

                if datecount <= 0:
                    raise NotFound('Invalid fromdate, todate')

                leave_allowance = LeaveAllowance.objects.get(employee__id=(
                    current_emp if employee is None else employee).id, leave_type__id=leave_type.id, created_date__year=datetime.date.today().year, del_flag=0)
                if leave_allowance is None:
                    raise NotFound('You don\'t have ' +
                                   leave_type.name + ' leave allowance.')
                if (leave_type.leave_days - leave_allowance.used_days - daycount) < 0:
                    raise BadRequest("You only have " + str(leave_type.leave_days -
                                     leave_allowance.used_days) + " days left for " + leave_type.name + " leave.")

                # Check if user requested leave in already exiting leaves
                # Check exiting leaves with status pending and cancel2
                leave_taken = Leave.objects.filter(Q(status=0) | Q(status=1),
                                                   leave_date__range=[from_date, to_date], del_flag=0, employee=(current_emp if employee is None else employee))
                if leave_taken.exists():
                    raise BadRequest(
                        message="You cannot request for already taken leave")

                while from_date <= to_date:
                    type = from_type if i == 1 else to_type if i == daycount else 0

                    leave_data = {
                        'employee': (current_emp if employee is None else employee),
                        'leave_type': leave_type,
                        'leave_date': convert_date_to_timestamp(from_date, is_str_date=False),
                        'supervisor': supervisor,
                        'status': request_data['status'],
                        'del_flag': 0,
                        'type': type

                    }
                    leave_serializer = LeaveSerializer(data=leave_data)
                    if leave_serializer.is_valid():
                        leave = leave_serializer.save()
                        if leave is not None:
                            leave_result.append(leave_serializer.data)

                    i = i + 1
                    from_date += delta
                if len(leave_result) > 0:
                    if(leave_data.get('status') == 0):
                        message = "Dear "+supervisor.name+",\n\n\tI kindly request you to review my leave request in In-House Attendance.\n\nSincerely,\n" + \
                            (current_emp if employee is None else employee).name + "\n" + (
                                current_emp if employee is None else employee).department.name + " Department"
                        send_mail(subject="Leave Request", message=message, from_email=(current_emp if employee is None else employee).email, recipient_list=[supervisor.email],
                                  fail_silently=False)
                    return ResponseUtil.success_response(result=leave_result, create=True,
                                                         model="Leave")
                raise NotFound()

    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except UnauthorizedException as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(e)


@api_view(['GET', 'POST'])
def leave(request):
    try:
        if request.method == "GET":
            # Initialize params
            employee_id = request.query_params.get('employee_id', None)
            supervisor_id = request.query_params.get('supervisor_id', None)
            month = int(request.query_params.get('month', None)) if request.query_params.get(
                'month', None) is not None else None
            year = int(request.query_params.get('year', None)) if request.query_params.get(
                'year', None) is not None else None
            employee = Employee.objects.get(
                pk=int(employee_id)) if employee_id is not None else None
            supervisor = Employee.objects.get(
                pk=int(supervisor_id)) if supervisor_id is not None else None

            # Define date range
            if month is not None:
                monthrange = calendar.monthrange(year, month)
            start_date = datetime.date(
                year, month, 1) if month is not None else datetime.date(year, 1, 1)
            end_date = datetime.date(
                year, month, monthrange[1]) + datetime.timedelta(days=1) if month is not None else datetime.date(year+1, 1, 1)

            if employee is not None and supervisor is not None:
                leave_filter = Leave.objects.filter(leave_date__range=[start_date, end_date], supervisor=supervisor,
                                                    employee=employee, del_flag=0)
                leave_serializer = LeaveSerializer(leave_filter, many=True)
                return ResponseUtil.success_response(result=leave_serializer.data, retrieve=True, model="Leave")
            if employee is not None:
                leave_filter = Leave.objects.filter(leave_date__range=[start_date, end_date],
                                                    employee=employee, del_flag=0)
                leave_serializer = LeaveSerializer(leave_filter, many=True)
                return ResponseUtil.success_response(result=leave_serializer.data, retrieve=True, model="Leave")
            if supervisor is not None:
                leave_filter = Leave.objects.filter(leave_date__range=[start_date, end_date],
                                                    supervisor=supervisor, del_flag=0)
                leave_serializer = LeaveSerializer(leave_filter, many=True)
                return ResponseUtil.success_response(result=leave_serializer.data, retrieve=True, model="Leave")
        if request.method == "POST":
            request_data = JSONParser().parse(request)
            try:
                employee = Employee.objects.get(
                    pk=request_data.get('employee_id'), del_flag=0)
            except Exception as e:
                raise NotFound(error=str(e), message="employee does not exit")
            try:
                supervisor = Employee.objects.get(
                    pk=request_data.get('supervisor_id'), del_flag=0)
            except Exception as e:
                raise NotFound(
                    error=str(e), message="Requested Supervisor does not exit")
            try:
                leave_type = LeaveType.objects.get(
                    pk=request_data.get('leave_type_id'), del_Flag=0)
            except Exception as e:
                raise NotFound(
                    error=str(e), message="Requested Leave type does not exit")
                # For Leave only one day
            if request_data.get('to_date') is None:
                from_date = timestamp_to_date(request_data.get('from_date'))
                # Check if user requested leave in holiday
                holiday = check_holiday(from_date, True)
                if holiday is not None:
                    raise BadRequest(message="Leave is unable in holiday")
                # Check if user requested leave in weekend
                weekend = is_it_off_day(from_date)
                if weekend:
                    raise BadRequest(message="Leave is unable  weekend")
                # Check if user requested leave in already exiting leaves
                # Check exiting leaves with status pending and cancel
                leave_taken = Leave.objects.filter(Q(status=0) | Q(status=1),
                                                   leave_date=from_date, del_flag=0, employee=employee)
                if leave_taken.exists():
                    raise BadRequest(message="already taken leave")
                leave_data = {
                    'employee': employee,
                    'leave_type': leave_type,
                    'leave_date': request_data.get('from_date'),
                    'supervisor': supervisor,
                    'status': request_data.get('status'),
                    'del_flag': 0,
                    'type': request_data.get('from_type')

                }
                leave_serializer = LeaveSerializer(data=leave_data)
                if leave_serializer.is_valid():
                    leave = leave_serializer.save()
                    if(leave_data.get('status') == 0):
                        message = "Dear "+supervisor.name+",\n\n\tI kindly request you to review my leave request in In-House Attendance.\n\nSincerely,\n" + \
                            employee.name + "\n" + employee.department.name + " Department"
                        send_mail(subject="Leave Request", message=message, from_email=employee.email, recipient_list=[supervisor.email],
                                  fail_silently=False)
                    return ResponseUtil.success_response(result=leave_serializer.data, create=True, model="Leave")

                # For Leaves more than one day
            else:
                from_date = timestamp_to_date(request_data.get('from_date'))
                to_date = timestamp_to_date(request_data.get('to_date'))
                delta = datetime.timedelta(days=1)  # for day iterate
                leave_result = []
                i = 1
                daycount = (to_date - from_date).days + 1
                while from_date <= to_date:
                    from_holiday = check_holiday(from_date, True)
                    to_holiday = check_holiday(to_date, True)
                    # Check if user requested leave in holiday
                    if from_holiday or to_holiday is not None:
                        raise BadRequest(
                            message="From Date or To Date is Holiday.Leave is unable")
                    # Check if user requested leave in weekend
                    from_weekend = is_it_off_day(from_date)
                    to_weekend = is_it_off_day(to_date)
                    if from_weekend or to_weekend:
                        raise BadRequest(
                            message="From Date or To Date is Weekend.Leave is unable")
                    # Check if user requested leave in already exiting leaves
                    # Check exiting leaves with status pending and cancel2
                    leave_taken = Leave.objects.filter(Q(status=0) | Q(status=1),
                                                       leave_date=from_date, del_flag=0, employee=employee)
                    if leave_taken.exists():
                        raise BadRequest(message="already taken leave")

                    type = request_data.get('from_type') if i == 1 else request_data.get(
                        'to_type') if i == daycount else 0
                    leave_data = {
                        'employee': employee,
                        'leave_type': leave_type,
                        'leave_date': convert_date_to_timestamp(from_date, is_str_date=False),
                        'supervisor': supervisor,
                        'status': request_data.get('status'),
                        'del_flag': 0,
                        'type': type

                    }
                    leave_serializer = LeaveSerializer(data=leave_data)
                    if leave_serializer.is_valid():
                        leave = leave_serializer.save()
                        leave_result.append(leave_serializer.data)
                    i = i + 1
                    from_date += delta
                if len(leave_result) > 0:
                    if(leave_data.get('status') == 0):
                        message = "Dear "+supervisor.name+",\n\n\tI kindly request you to review my leave request in In-House Attendance.\n\nSincerely,\n" + \
                            employee.name + "\n" + employee.department.name + " Department"
                        send_mail(subject="Leave Request", message=message, from_email=employee.email, recipient_list=[supervisor.email],
                                  fail_silently=False)
                    return ResponseUtil.success_response(result=leave_result, create=True,
                                                         model="Leave")

    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        print(e)
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['PUT', 'POST'])
def leave_modified(request, pk):
    try:
        try:
            leave = Leave.objects.get(pk=pk)
        except Exception as e:
            raise NotFound(error=str(e), message="leave does not exit")

        if request.method == 'PUT':
            leave_data = JSONParser().parse(request)
            employee_id = leave_data.get('employee')['id']
            leave_type_id = leave_data.get('leave_type')['id']
            employee = Employee.objects.get(pk=employee_id)
            leave_type = LeaveType.objects.get(pk=leave_type_id)
            emp_leave_allowance = LeaveAllowance.objects.get(
                employee=employee, leave_type=leave_type, del_flag=0, created_date__year=datetime.date.today().year)
            if leave_data['status'] == 1 and emp_leave_allowance.leave_type.leave_days < (emp_leave_allowance.used_days + (1 if leave.type == 0 else 0.5)):
                raise BadRequest(
                    message="Employee Leave allowances is full of for this leave type")
            else:
                leave_serializer = LeaveSerializer(leave, data=leave_data)
                if leave_serializer.is_valid():
                    leave_serializer.save()
                    used_days = emp_leave_allowance.used_days
                    if leave_data['status'] == 1:
                        emp_leave_allowance.used_days = used_days + \
                            1 if leave.type == 0 else used_days + 0.5
                        emp_leave_allowance.save()
                    return ResponseUtil.success_response(result=leave_serializer.data, update=True, model="Leave")
                else:
                    raise BadRequest(message=leave_serializer.errors)

        elif request.method == 'POST':
            leave.del_flag = 1
            leave.save()
            emp_leave_allowance = LeaveAllowance.objects.get(
                employee=leave.employee, leave_type=leave.leave_type, del_flag=0, created_date__year=datetime.date.today().year)
            if leave.status == 1 and emp_leave_allowance.used_days > 0:
                print(LeaveSerializer(leave).data)
                emp_leave_allowance.used_days -= 1 if leave.type == 0 else 0.5
                emp_leave_allowance.save()
            return ResponseUtil.success_response(result={}, delete=True, model="Leave")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))
