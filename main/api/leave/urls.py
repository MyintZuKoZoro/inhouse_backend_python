from django.urls import path
from main.api.leave import views

urlpatterns = [
     path('', views.leave),
     path('/request', views.leave_request),
     path('/<int:pk>', views.leave_modified),

]
