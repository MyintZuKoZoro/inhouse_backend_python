
from django.db import models
from datetime import datetime
from main.api.employee.models import Employee
from main.api.leave_type.models import LeaveType


class Leave(models.Model):
    id = models.AutoField(primary_key=True)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='employee')
    leave_type = models.ForeignKey(LeaveType, on_delete=models.CASCADE)
    leave_date = models.DateField()
    leave_reason = models.CharField(max_length=500 , null= True)
    supervisor = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name='request_to')
    status = models.IntegerField(default=0)
    del_flag = models.IntegerField(default=0)
    type = models.IntegerField(default=0)
    created_id = models.CharField(max_length=50, blank=False , default = 0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.CharField(max_length=50, blank=False ,default = 0)
    updated_date = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "tbl_leave"

