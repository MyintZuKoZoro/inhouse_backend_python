from django.db import models

from main.api.employee.models import Employee
import datetime


class Supervisor(models.Model):
    emp = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="employees")
    supervisor = models.ForeignKey(Employee, on_delete=models.CASCADE, related_name="supervisors")
    created_id = models.IntegerField(default=0)
    updated_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.datetime.now)
    updated_date = models.DateTimeField(default=datetime.datetime.now)
    del_flag = models.IntegerField(default=0)

    class Meta:
        db_table = "tbl_supervisors"
