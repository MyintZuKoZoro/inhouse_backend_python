from rest_framework import serializers

from main.api.employee.serializers import EmployeeSerializer
from main.api.supervisor.models import Supervisor
from main.serializers import JsTimeStampFieldForDateTime


class SupervisorSerializer(serializers.ModelSerializer):
    emp = EmployeeSerializer(read_only=True)
    supervisor = EmployeeSerializer(read_only=True)
    created_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)
    updated_date = JsTimeStampFieldForDateTime(allow_null=True, required=False)

    class Meta:
        model = Supervisor
        fields = "__all__"
