from django.http import JsonResponse

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from main.api.employee.models import Employee
from main.api.supervisor.models import Supervisor

from main.api.employee.serializers import EmployeeSerializer
from main.api.supervisor.serializers import SupervisorSerializer
from main.response import ApiResponse
from main.util.CommonUtil import get_user_from_token

from main.util.ExceptionUtil import UnauthorizedException, InternalServerError, BadRequest, NotFound
from main.util.ResponseUtil import ResponseUtil


@api_view(['GET', 'POST', 'DELETE'])
def supervisorRequest(request):
    try:
        current_user = get_user_from_token(request)
        if request.method == "GET":
            emp_id = int(request.GET.get("emp_id"))
            op_type = int(request.GET.get('type'))
            if op_type is None:
                op_type = 0
            # op_type 0 for supervisor 1 for normal
            try:
                employee = Employee.objects.get(pk=emp_id)
            except Exception as e:
                raise NotFound(message=str(e))
            if op_type == 0:
                supervisor_object_list = Supervisor.objects.filter(
                    emp=employee)
                employee_object_list = []
                for supervisor_object in supervisor_object_list:
                    employee_object_list.append(supervisor_object.supervisor)
                employee_serialized_list = EmployeeSerializer(
                    employee_object_list, many=True) if len(employee_object_list) > 0 else None
                return ResponseUtil.success_response(result=employee_serialized_list.data if employee_serialized_list is not None else [], retrieve=True,
                                                     model="Supervisor-Employee")
            if op_type == 1:
                supervisor_object_list = Supervisor.objects.filter(
                    supervisor=employee)
                employee_object_list = []
                for supervisor_object in supervisor_object_list:
                    employee_object_list.append(supervisor_object.emp)
                employee_serialized_list = EmployeeSerializer(
                    employee_object_list, many=True) if len(employee_object_list) > 0 else None
                return ResponseUtil.success_response(result=employee_serialized_list.data if employee_serialized_list is not None else [], retrieve=True,
                                                     model="Supervisor-Employee")
        #
        # employee = get_user_from_token(request)
        # isAuthorizedPerson = check_is_authorized_person(request, employee)

        if request.method == "POST":
            # if isAuthorizedPerson:
            request_body = JSONParser().parse(request)
            if request_body['employee_id'] is None or request_body['supervisor_id'] is None:
                raise BadRequest(
                    message="supervisor_id and employee_id must include")
            employee = Employee.objects.get(id=request_body['employee_id'])
            supervisor = Employee.objects.get(id=request_body['supervisor_id'])
            the_supervisor = Supervisor(emp=employee, supervisor=supervisor, created_id=current_user.id,
                                        updated_id=current_user.id)
            the_supervisor.save()
            result = SupervisorSerializer(the_supervisor)
            return ResponseUtil.success_response(result=result.data, create=True, model="Supervisor-Employee")

        else:
            # if isAuthorizedPerson:
            emp_id = int(request.GET.get('emp_id')) if request.GET.get(
                'emp_id') is not None else None
            supervisor_id = int(request.GET.get('supervisor_id')) if request.GET.get(
                'supervisor_id') is not None else None
            if emp_id is None or supervisor_id is None:
                raise BadRequest(
                    message="emp_id and supervisor_id must include")
            try:
                the_supervisor = Supervisor.objects.get(
                    emp_id=emp_id, supervisor_id=supervisor_id)
            except Exception as e:
                raise NotFound(message=str(e))
            supervisor = the_supervisor.supervisor
            the_supervisor.delete()
            return ResponseUtil.success_response(result=None, delete=True, model="Supervisor-Employee")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))
