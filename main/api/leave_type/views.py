from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated

from main.api.leave_allowance.views import crud_leave_allowance_by_leave_type
from main.api.leave_type.models import LeaveType
from main.api.leave_type.serializers import LeaveTypeSerializer
from main.util.CommonUtil import get_user_from_token
from main.util.ExceptionUtil import BadRequest, InternalServerError, NotFound
from main.util.ResponseUtil import ResponseUtil


@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def leave_type(request):
    try:
        if request.method == 'GET':
            # TODO filter by keyword
            leave_type_lists = LeaveType.objects.filter(del_flag=0)
            leave_type_serializer = LeaveTypeSerializer(
                leave_type_lists, many=True)
            return ResponseUtil.success_response(result=leave_type_serializer.data, retrieve=True, model='Leave Type')

        elif request.method == 'POST':
            leave_type_data = JSONParser().parse(request)
            # current user
            current_user = get_user_from_token(request)
            if not ('created_id' in leave_type_data):
                leave_type_data['created_id'] = int(current_user.id)
            if not ('updated_id' in leave_type_data):
                leave_type_data['updated_id'] = int(current_user.id)

            leave_type_serializer = LeaveTypeSerializer(data=leave_type_data)
            if leave_type_serializer.is_valid():
                leave_type_serializer.save()
                # after created leave type, to create leave allowance by leave_type_id and employee_id
                crud_leave_allowance_by_leave_type(
                    request, leave_type_serializer.data, 'CREATE')
                return ResponseUtil.success_response(result=leave_type_serializer.data, create=True,
                                                     model='Leave Type')

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def leave_type_modified(request, pk):
    try:
        # common & retrieve leave type by filter
        try:
            current_user = get_user_from_token(request)
            leave_type_list = LeaveType.objects.get(id=pk, del_flag=0)
        except Exception as error:
            raise NotFound(error=str(error))

        if request.method == 'GET':
            leave_type_serializer = LeaveTypeSerializer(leave_type_list)
            return ResponseUtil.success_response(result=leave_type_serializer.data, retrieve=True, model='Leave Type')

        elif request.method == 'PUT':
            leave_type_data = JSONParser().parse(request)
            leave_type_data['updated_id'] = current_user.id
            leave_type_serializer = LeaveTypeSerializer(
                leave_type_list, data=leave_type_data)
            if leave_type_serializer.is_valid():
                leave_type_serializer.update(
                    leave_type_list, leave_type_serializer.validated_data)
                # after updated leave type, to update leave allowance by leave_type_id and employee_id
                crud_leave_allowance_by_leave_type(
                    request, leave_type_serializer.data, 'UPDATE')
                return ResponseUtil.success_response(result=leave_type_serializer.data, update=True,
                                                     model='Leave Type')
            else:
                raise BadRequest()

        elif request.method == 'POST':
            leave_type_list.del_flag = 1
            leave_type_list.update_id = current_user.id
            leave_type_list.save()
            leave_type_serializer = LeaveTypeSerializer(leave_type_list)
            # after del_flag=0 leave type, to update leave allowance del_flag=0 by leave_type_id and employee_id
            crud_leave_allowance_by_leave_type(
                request, leave_type_serializer.data, 'DELETE')
            return ResponseUtil.success_response(result=leave_type_serializer.data, delete=True,
                                                 model='Leave Type')

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))
