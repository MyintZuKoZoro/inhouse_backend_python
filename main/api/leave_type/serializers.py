import datetime

from rest_framework import serializers
from main.api.leave_type.models import LeaveType


class LeaveTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeaveType
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.leave_days = validated_data.get(
            'leave_days', instance.leave_days)
        instance.valid_at = validated_data.get('valid_at', instance.valid_at)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.del_flag = validated_data.get('del_flag', instance.del_flag)
        instance.updated_date = datetime.datetime.now()
        instance.save()
        return instance
