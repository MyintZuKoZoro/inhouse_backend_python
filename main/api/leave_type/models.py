from datetime import datetime

from django.db import models


class LeaveType(models.Model):
    name = models.CharField(max_length=50, blank=False)
    leave_days = models.FloatField(default=0)
    valid_at = models.IntegerField(default=0)
    description = models.CharField(max_length=250, default='', blank=True)
    del_flag = models.IntegerField(default=0)
    created_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "tbl_leave_type"
