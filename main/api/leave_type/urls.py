from django.urls import path
from main.api.leave_type import views

urlpatterns = [
    path('', views.leave_type),
    path('/<int:pk>', views.leave_type_modified)
]
