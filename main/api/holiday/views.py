from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated

from main.util.CommonUtil import get_user_from_token
from main.util.ExceptionUtil import BadRequest, NotFound, InternalServerError
from main.util.ResponseUtil import ResponseUtil

from main.api.holiday.models import Holiday
from main.api.holiday.serializers import HolidaySerializer


@api_view(['GET', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def holiday(request):
    try:
        if request.method == 'GET':
            # TODO filter by keyword
            # holiday_lists = Holiday.objects.filter(name=request.GET.get('name'))
            # print(holiday_lists)
            holiday_list = Holiday.objects.filter(del_flag=0)
            holiday_serializer = HolidaySerializer(holiday_list, many=True)
            return ResponseUtil.success_response(result=holiday_serializer.data, retrieve=True, model='Holiday')

        elif request.method == 'POST':
            holiday_data = JSONParser().parse(request)
            # current user
            current_user = get_user_from_token(request)
            if not ('created_id' in holiday_data):
                holiday_data['created_id'] = int(current_user.id)
            if not ('updated_id' in holiday_data):
                holiday_data['updated_id'] = int(current_user.id)

            holiday_serializer = HolidaySerializer(data=holiday_data)
            if holiday_serializer.is_valid():
                holiday_serializer.save()
                return ResponseUtil.success_response(result=holiday_serializer.data, create=True, model='Holiday')

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))


@api_view(['GET', 'PUT', 'POST'])
@authentication_classes([TokenAuthentication])
@permission_classes([IsAuthenticated])
def holiday_modified(request, pk):
    try:
        # common & retrieve leave type by filter
        try:
            current_user = get_user_from_token(request)
            holiday_list = Holiday.objects.get(id=pk, del_flag=0)
        except Exception as error:
            raise NotFound(error=str(error))

        if request.method == 'GET':
            holiday_serializer = HolidaySerializer(holiday_list)
            return ResponseUtil.success_response(result=holiday_serializer.data, retrieve=True, model='Holiday')

        elif request.method == 'PUT':
            holiday_data = JSONParser().parse(request)
            if not ('updated_id' in holiday_data):
                holiday_data['updated_id'] = current_user.id

            holiday_serializer = HolidaySerializer(
                holiday_list, data=holiday_data)
            if holiday_serializer.is_valid():
                holiday_serializer.update(
                    holiday_list, holiday_serializer.validated_data)
                return ResponseUtil.success_response(result=holiday_serializer.data, update=True, model='Holiday')

        elif request.method == 'POST':
            holiday_list.del_flag = 1
            holiday_list.updated_id = current_user.id
            holiday_list.save()
            result = HolidaySerializer(holiday_list).data
            return ResponseUtil.success_response(result=result, delete=True, model='Holiday')

    except BadRequest as error:
        return ResponseUtil.error_response(error)
    except NotFound as error:
        return ResponseUtil.error_response(error)
    except Exception as error:
        return ResponseUtil.error_response(InternalServerError(error=str(error)))

# TODO:
# create > attendance < = > holiday
