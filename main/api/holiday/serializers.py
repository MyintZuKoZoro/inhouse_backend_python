import datetime

from rest_framework import serializers
from main.api.holiday.models import Holiday
from main.serializers import JsTimeStampFieldForDate


class HolidaySerializer(serializers.ModelSerializer):
    date = JsTimeStampFieldForDate()

    class Meta:
        model = Holiday
        fields = '__all__'

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.date = validated_data.get('date', instance.date)
        instance.del_flag = validated_data.get('del_flag', instance.del_flag)
        instance.updated_date = datetime.datetime.now()
        instance.save()
        return instance