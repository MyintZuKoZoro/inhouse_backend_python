from django.urls import path
from main.api.holiday import views

urlpatterns = [
    path('', views.holiday),
    path('/<int:pk>', views.holiday_modified)
]
