from datetime import datetime, date

from django.db import models


class Holiday(models.Model):
    name = models.CharField(max_length=50, blank=False)
    date = models.DateField(default=date.today)
    del_flag = models.IntegerField(default=0)
    created_id = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=datetime.now)
    updated_id = models.IntegerField(default=0)
    updated_date = models.DateTimeField(default=datetime.now)

    # created_id = models.ForeignKey(Employee, on_delete=models.CASCADE,
    #                                related_name="holiday_created_id",
    #                                null=True, blank=True)
    # created_date = models.DateTimeField(default=datetime.now, blank=False)
    # updated_id = models.ForeignKey(Employee, on_delete=models.CASCADE,
    #                                related_name="holiday_updated_id",
    #                                null=True, blank=True)
    # updated_date = models.DateTimeField(default=datetime.now, blank=False)

    class Meta:
        db_table = "tbl_holiday"
