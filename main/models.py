from django.db import models
# Create your models here.
from main.api.department.models import Department
from main.api.employee.models import Employee
from main.api.holiday.models import Holiday
from main.api.leave_type.models import LeaveType
from main.api.supervisor.models import Supervisor
from main.api.leave_allowance.models import LeaveAllowance
from main.api.leave.models import Leave
from main.api.overtime.models import Overtime
from main.api.attendance.models import Attendance