from rest_framework import serializers
from rest_framework.authtoken.models import Token
from authentication.models import User
from main.api.attendance.models import Attendance
from main.api.attendance.serializers import AttendanceSerializer
from main.api.employee.models import Employee
from main.api.holiday.models import Holiday
from main.api.holiday.serializers import HolidaySerializer
from main.api.leave.models import Leave
from main.api.leave.serializers import LeaveSerializer
from main.api.overtime.models import Overtime
from main.api.overtime.serializers import OvertimeSerializer
from main.api.supervisor.models import Supervisor
import time
import datetime
import re


def has_number_in_string(string_data):
    return bool(re.search(r'\d', string_data))


# return employee from token
def get_user_from_token(request):
    try:
        token = request.headers["Authorization"][6:]
        token_object = Token.objects.get(key=token)
        user = User.objects.get(pk=token_object.user_id)
        employee = Employee.objects.get(pk=user.emp_id)
        return employee
    except Exception as error:
        return None


# if emp role is 1 or 0 return True else check supervisor_emp relationship

def check_is_authorized_person(supervisor, emp):
    check_supervisor_rs = Supervisor.objects.filter(emp=emp,
                                                    supervisor=supervisor)
    if len(check_supervisor_rs) > 0:
        print('has supervisor emp relation')
        return True
    else:
        print('does not have supervisor emp relation')
        return False


# return list of systemholidays
def get_system_holidays():
    holiday_list = Holiday.objects.filter(del_Flag=0)
    return holiday_list


# pass string return datetime
def convert_str_to_datetime(str_date):
    result_datetime = datetime.datetime.strptime(
        str_date[:19] + 'Z', '%Y-%m-%dT%H:%M:%SZ')
    print("result from str to datetime is {} ".format(result_datetime))
    return result_datetime


# pass string return date
def convert_str_to_date(str_date):
    result_date = datetime.datetime.strptime(str_date, '%Y-%m-%d').date()
    return result_date


# pass date return string
def convert_date_to_str(date, date_time_format=True):
    result_str = date.strftime(
        '%Y-%m-%dT%H:%M:%SZ' if date_time_format else '%Y-%m-%d')
    return result_str


def check_leave(str_date, emp):
    try:
        date = convert_str_to_date(str_date)
        leave = Leave.objects.get(
            leave_date=date, employee=emp, del_flag=0, status=1)
        leave_serializer = LeaveSerializer(leave)
        return leave_serializer.data
    except Exception:
        return None


# pass string_date retrun holiday object at that date
def check_holiday(str_date, is_it_datetime=True):
    try:
        # if str_date is datetime format then change to datetime format else date format
        if is_it_datetime:
            date_time = convert_str_to_datetime(str_date)
        else:
            date_time = convert_str_to_date(str_date)
        # month and day
        holiday = Holiday.objects.get(date=date_time, del_flag=0)
        if holiday is not None:
            return HolidaySerializer(holiday).data
        else:
            return None
    except Exception as e:
        return None


# pass date or emp return overtime object
def check_overtime(str_date, emp, request_emp=None):
    try:
        ot_date = convert_str_to_date(str_date)
        if request_emp is None:
            if emp is None:
                overtime = Overtime.objects.get(
                    date=ot_date, status=1, del_flag=0)
            else:
                overtime = Overtime.objects.get(
                    date=ot_date, emp=emp, status=1, del_flag=0)
        else:
            if emp is None:
                overtime = Overtime.objects.get(
                    date=ot_date, request_emp=request_emp)
            else:
                overtime = Overtime.objects.get(
                    date=ot_date, emp=emp, request_emp=request_emp)
        overtime_serializer = OvertimeSerializer(overtime)
        result = overtime_serializer.data
        return result
    except Exception as e:
        return None


# pass timestamp return boolean
def compare_present_and_timestamp(timestamp_data):
    date = datetime.datetime.fromtimestamp(timestamp_data).date()
    if date < datetime.datetime.now().date():
        return False
    return True


def compare_timestamp_and_date(timestamp_data, date_data):
    timestamp_date = datetime.datetime.fromtimestamp(timestamp_data).date()
    if timestamp_date == date_data:
        return True
    return False


# pass date and return boolean
def is_it_Today(date_data):
    if date_data < datetime.datetime.now().date():
        return False
    return True


# pass string date return attendance object
def check_attendance(str_date, emp):
    try:
        at_date = convert_str_to_date(str_date)
        if emp is None:
            attendance = Attendance.objects.get(date=at_date, del_flag=0)
        else:
            attendance = Attendance.objects.get(
                date=at_date, emp=emp, del_flag=0)
        # if request want as timestmap just
        attendance_serializer = AttendanceSerializer(attendance)
        serialized_data = attendance_serializer.data
        return serialized_data
    except Exception as e:
        print(str(e))
        return None


# check is it sunday or saturday
def is_it_off_day(date_data):
    print(date_data.weekday())
    if date_data.weekday() == 5 or date_data.weekday() == 6:
        return True
    return False


# pass datetime get time
def get_time_from_datetime(datetime_data):
    str_time = datetime_data.strftime('%H:%M:%S')
    result_time = datetime.datetime.strptime(str_time, '%H:%M:%S').time()
    return result_time


# used
def compare_date_and_datetime(datetime_data, date_data):
    if date_data is not None and datetime_data is not None:
        if datetime_data.date() == date_data:
            return True
    return False


# timestamp to date converter
def timestamp_to_date(timestamp):
    return datetime.datetime.fromtimestamp(int(timestamp) / 1000).date()


# timestamp to datetime converter
def timestamp_to_datetime(timestamp):
    return datetime.datetime.fromtimestamp(int(timestamp) / 1000)


# datetime to timestamp converter
def convert_datetime_to_timestamp(datetime_data):
    ts = datetime_data.timestamp()
    return ts * 1000


# date to timestamp converter , check date_data date or string and default is string param
def convert_date_to_timestamp(date_data, is_str_date=True):
    result_timestamp = time.mktime(
        (date_data if not is_str_date else convert_str_to_date(date_data)).timetuple())
    return result_timestamp * 1000


working_start_time = datetime.time(8, 0, 0)
working_end_time = datetime.time(17, 0, 0)


# Not use anymore
# ############################################################################################################################3
def convert_all_datetime_to_timestamp_in_object(dict_data, *fields_to_change):
    for x in fields_to_change:
        for y in dict_data:
            if y == x[0] and dict_data[y] is not None:
                dict_data[y] = convert_date_to_timestamp(dict_data[y]) * 1000 if x[
                    1] == "date" else convert_datetime_to_timestamp(
                    convert_str_to_datetime(dict_data[y])) * 1000
    return dict_data


def convert_all_timestamp_to_datetime_in_object(dict_data, *fields_to_change):
    print('changing to datetime')

    for x in fields_to_change:

        for y in dict_data:

            if y == x[0] and dict_data[y] is not None:
                print("found at {} and {}".format(y, x[0]))

                print(dict_data[y])
                dict_data[y] = convert_date_to_str(datetime.datetime.fromtimestamp(int(dict_data[y]) / 1000).date(),
                                                   False) if x[
                    1] == "date" else convert_date_to_str(
                    datetime.datetime.fromtimestamp(int(dict_data[y]) / 1000))
