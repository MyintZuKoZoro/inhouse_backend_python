from re import match

from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response


class ResponseUtil:

    @staticmethod
    def message_response(result, message):
        response_data = {
            "success": True,
            "result": result,
            "code": 200,
            "message": message,
            "error": False
        }
        return Response(response_data, status=status.HTTP_200_OK)

    @staticmethod
    def error_response(e=None, error_type=None, error=None):
        if error_type is not None and error is not None:
            response_data = {"code": error_type, "success": False,
                             "message": error, "error": error}
            return Response(response_data, status=error_type)
        response_data = {"code": e.code, "success": False,
                         "message": e.message, "error": e.error}
        return Response(response_data, status=e.status)

    @staticmethod
    def success_response(result, delete=False, update=False, retrieve=False, create=False, model="Object"):
        message_op = "Deleted" if delete else "Updated" if update else "Retrieved" if retrieve else "Created"
        status_code = status.HTTP_201_CREATED if create else status.HTTP_200_OK
        response_data = {
            'code': status_code,
            'success': True,
            'message': message_op + ' ' + model + ' ' + "Successfully",
            'result': result,
            'error': False
        }
        return Response(response_data, status=status_code)

    # Not Use Any More Functions
    # ###########################################################################################################
    @staticmethod
    def api_response(code, success, message, result, error):
        response_Data = {
            "code": code,
            "success": success,
            "message": message,
            "result": result,
            "error": error,
        }
        return Response(response_Data)

    @staticmethod
    def unauthorized_response(message='UNAUTHORIZED REQUEST'):
        response_data = {
            'code': status.HTTP_401_UNAUTHORIZED,
            'success': False,
            'message': message,
            'error': True
        }
        return Response(response_data)

    @staticmethod
    def internal_server_response(message='INTERNAL SERVER ERROR'):
        response_data = {
            'code': status.HTTP_500_INTERNAL_SERVER_ERROR,
            'success': False,
            'message': message,
            'error': True
        }
        return Response(response_data)

    @staticmethod
    def not_found_response(message='RESOURCE NOT FOUND'):
        response_data = {
            'code': status.HTTP_404_NOT_FOUND,
            'success': False,
            'message': message,
            'error': True
        }
        return Response(response_data)
