from rest_framework import status


class NotFound(Exception):
    def __init__(self, message="RESOURCE NOT FOUND", error=None):
        self.message = message
        self.status = status.HTTP_404_NOT_FOUND
        self.code = 404
        self.error = error


class InternalServerError:
    def __init__(self, message='INTERNAL SERVER ERROR', error='INTERNAL SERVER ERROR'):
        self.message = message
        self.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        self.code = 500
        self.error = error


class UnauthorizedException(Exception):
    def __init__(self, message="UNAUTHORIZED REQUEST", error="UNAUTHORIZED REQUEST"):
        self.message = message
        self.status = status.HTTP_401_UNAUTHORIZED
        self.code = 401
        self.error = error


class ForBiddenError(Exception):
    def __init__(self, message="FORBIDDEN REQUEST", error="FORBIDDEN REQUEST"):
        self.message = message
        self.status = status.HTTP_403_FORBIDDEN
        self.code = 403
        self.error = error


class BadRequest(Exception):
    def __init__(self, message="BAD REQUEST", error="BAD REQUEST"):
        self.message = message
        self.status = status.HTTP_403_FORBIDDEN
        self.code = 400
        self.error = error
