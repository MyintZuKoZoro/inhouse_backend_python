from rest_framework import status


class ApiResponse:
    def formatResponse(result, success, code, error, message):
        response_data = {}
        response_data['result'] = result
        response_data['message'] = message
        response_data['success'] = success
        response_data['code'] = code
        response_data['error'] = error
        return response_data

    # tzt

    def successResponse(result, message="Successfully operated"):
        response_data = {'result': result, "message": message, 'success': True, "code": status.HTTP_200_OK,
                         "error": False}
        return response_data

    def unAuthorizedRespose(message="Unauthorized Request"):
        response_data = { "message": message, "code":status.HTTP_401_UNAUTHORIZED}
        return response_data

    def internalServerResponse(message="Internal Server Error"):
        response_data = {"message": message, "code":status.HTTP_500_INTERNAL_SERVER_ERROR}
        return response_data

    def notFoundResponse(message="Not Found"):
        response_data= {"message": message, "code": status.HTTP_404_NOT_FOUND, "success": False, "error": True}
        return response_data