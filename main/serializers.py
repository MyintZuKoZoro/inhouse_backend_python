
from rest_framework import serializers

from main.util import CommonUtil
import datetime


class JsTimeStampFieldForDateTime(serializers.Field):
    def to_representation(self, value):
        return CommonUtil.convert_datetime_to_timestamp(value)

    def to_internal_value(self, data):
        return datetime.datetime.fromtimestamp(data / 1000)


class JsTimeStampFieldForDate(serializers.Field):
    def to_representation(self, value):
        return CommonUtil.convert_date_to_timestamp(value, False)

    def to_internal_value(self, data):
        return datetime.datetime.fromtimestamp(data / 1000).date()
