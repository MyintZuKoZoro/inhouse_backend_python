from rest_framework import serializers
from rest_framework.authtoken.models import Token

from authentication.models import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create_user_and_get_token(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
            emp_id=validated_data['emp_id']

        )
        print("user_id")
        print(user)
        token, created = Token.objects.get_or_create(user=user)
        return token.key

    class Meta:
        model = User
