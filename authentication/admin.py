from django.contrib import admin
from django.contrib import admin
import main.models
# Register your models here.
from authentication.models import User
from main.api.attendance.models import Attendance
from main.api.department.models import Department
from main.api.employee.models import Employee
from main.api.holiday.models import Holiday
from main.api.leave.models import Leave
from main.api.leave_allowance.models import LeaveAllowance
from main.api.leave_type.models import LeaveType
from main.api.overtime.models import Overtime
from main.api.supervisor.models import Supervisor

admin.site.register(User)
admin.site.register(Department)
admin.site.register(Employee)
admin.site.register(Attendance)
admin.site.register(Holiday)
admin.site.register(Leave)
admin.site.register(LeaveAllowance)
admin.site.register(LeaveType)
admin.site.register(Overtime)
admin.site.register(Supervisor)

