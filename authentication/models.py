from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
import datetime

# Create your models here.
from main.api.department.models import Department
from main.api.employee.models import Employee


class UserManager(BaseUserManager):

    def create_user(self, email, username, emp_id, password=None):
        if email is None:
            raise TypeError('Users should have email')
        if username is None:
            raise TypeError('Users should have username')
        user = self.model(email=self.normalize_email(email), emp_id=emp_id, username=username)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password=None):
        if username is None:
            raise TypeError('Superuser should have username')
        user = self.model(username=username)
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=225, unique=True, db_index=True, default='')
    email = models.EmailField(max_length=225, db_index=True, unique=True, null=True, blank=True)
    del_flag = models.IntegerField(default=0)
    verification_code = models.CharField(max_length=10, null=True, blank=True)
    emp = models.OneToOneField(to=Employee, related_name="user_info", on_delete=models.CASCADE, blank=True, null=True, unique=True)
    is_staff = models.BooleanField(default=False)
    vcode_expiration_datetime = models.DateTimeField(null=True, blank=True)
    USERNAME_FIELD = 'username'
    objects = UserManager()
