import random

from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from authentication.models import User
from inhouse_backend.settings import EMAIL_HOST_USER
from main.api.employee.models import Employee
from main.api.employee.serializers import EmployeeSerializer
from main.response import ApiResponse
from django.core.mail import send_mail

import datetime

from main.util.ResponseUtil import ResponseUtil
from main.util.ExceptionUtil import InternalServerError, NotFound, BadRequest
import pytz

utc = pytz.UTC


@api_view(['POST'])
def login(request):
    try:
        request_data = JSONParser().parse(request)
        username = request_data['username']
        password = request_data['password']
        user = authenticate(username=username, password=password)
        if user is None:
            raise NotFound(message="Username and Password Not Match")
        employee = Employee.objects.get(pk=user.emp_id)
        emp_serializer = EmployeeSerializer(employee)
        token, created = Token.objects.get_or_create(user=user)
        response_data = {"data": emp_serializer.data, "token": token.key}
        return ResponseUtil.success_response(response_data, "Login Success")
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST'])
def forgetPassword(request):
    try:
        request_data = JSONParser().parse(request)
        email = request_data['email']
        try:
            user = User.objects.get(email=email)
        except Exception as e:
            raise NotFound(
                message="User with this email does not exist in system", error=str(e))
        random_number = ''
        for i in range(6):
            temp_rd_number = str(random.randint(0, 9))
            random_number = random_number + temp_rd_number

        user.verification_code = random_number
        user.vcode_expiration_datetime = (datetime.datetime.now() +
                                          datetime.timedelta(minutes=10)).replace(tzinfo=utc)
        user.save()
        send_mail("Verification Code",
                  "Your Verification Code is {} ".format(
                      random_number), EMAIL_HOST_USER, [email],
                  fail_silently=False)
        return ResponseUtil.message_response(None, "We send verification code to {} ".format(email))
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST'])
def validateVerificationCode(request):
    try:
        request_data = JSONParser().parse(request)
        try:
            user = User.objects.get(
                email=request_data['email'], verification_code=request_data["verification_code"])
        except Exception as e:
            raise NotFound(message="Wrong Validation Code", error=str(e))
        if user.vcode_expiration_datetime is not None and user.vcode_expiration_datetime >= datetime.datetime.now().replace(tzinfo=utc):
            return ResponseUtil.message_response(None, "Validation Success")
        else:
            raise BadRequest(message="Your Validation Code is Expired")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        print(e)
        return ResponseUtil.error_response(InternalServerError(error=str(e)))


@api_view(['POST'])
def resetPassword(request):
    try:
        request_data = JSONParser().parse(request)
        try:
            user = User.objects.get(
                email=request_data['email'], verification_code=request_data["verification_code"])
        except Exception as e:
            raise NotFound(
                message="Email and Verification Code do not match", error=str(e))
        if user.vcode_expiration_datetime is not None and user.vcode_expiration_datetime >= datetime.datetime.now().replace(tzinfo=utc):
            user.set_password(request_data["password"])
            user.save()
            return ResponseUtil.success_response(None, update="True", model="User")
        else:
            raise BadRequest(message="Your Validation Code is Expired")
    except BadRequest as e:
        return ResponseUtil.error_response(e)
    except NotFound as e:
        return ResponseUtil.error_response(e)
    except Exception as e:
        return ResponseUtil.error_response(InternalServerError(error=str(e)))
