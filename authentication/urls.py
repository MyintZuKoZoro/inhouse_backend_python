from django.urls import path
from authentication.views import *
urlpatterns = [
    path('login', login),
    path('forget-password', forgetPassword),
    path('reset-password', resetPassword),
    path('validate-vcode', validateVerificationCode)
]
