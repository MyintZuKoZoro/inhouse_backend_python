# from django.http import JsonResponse
# from rest_framework.utils import json
#
# from main.api.attendance.models import Attendance
# from main.api.employee.models import Employee
# from main.api.overtime.models import Overtime
# from main.response import ApiResponse
# from main.util.CommonUtil import get_user_from_token, check_is_authorized_person, has_number_in_string
# from main.util.ExceptionUtil import UnauthorizedException
#
#
# class SystemMiddleware:
#     def __init__(self, get_response):
#         self.get_response = get_response
#         # One-time configuration and initialization.
#
#     def __call__(self, request):
#         ref_request = request
#         # Code to be executed for each request before
#         # the view (and later middleware) are called.
#         try:
#             print(request.get_full_path())
#             if "/auth/" not in request.get_full_path() and "Authorization" not in request.headers:
#                 raise UnauthorizedException(message="You are unauthorized to fetch system Data")
#             current_user = get_user_from_token(request)
#             if current_user.role != 1 or current_user.role != 0:
#                 # supervisor BL
#                 if "/api/supervisor" in request.get_full_path():
#                     # if current user has either role 1 or 0 then let this request authorized
#                     if request.method == "POST" or request.method == "DELETE":
#                         raise UnauthorizedException(message="You Don't have Permission to Do that")
#                 # overtime BL
#                 if "/api/overtime" in request.get_full_path():
#                     if "/api/overtime/request" == request.get_full_path():
#                         # if current_user has either role 1 or 0 let him authorized
#                         # else has to check supervisor_to_emp relationship
#                         request_body = json.loads(request.body)
#                         print(request_body)
#                         request_emp = Employee.objects.get(pk=request_body['request_emp']['id'])
#                         print(request_emp.role)
#                         if request_emp.role == 2:
#                             if not check_is_authorized_person(request_emp, current_user):
#                                 raise Exception('Overtime request cant send to this person')
#                     if "/api/overtime" == request.get_full_path():
#                         print('overtime get post validation')
#                         if request.method == "GET":
#                             emp_id = request.GET.get("emp_id")
#                             request_id = request.GET.get("request_id")
#                             if emp_id is not None:
#                                 emp = Employee.objects.get(pk=int(emp_id))
#                                 if emp != current_user:
#                                     if not check_is_authorized_person(current_user, emp):
#                                         raise UnauthorizedException(message="You are Unauthorized to "
#                                                                             "fetch someone else Overtime")
#                             if request_id is not None:
#                                 supervisor = Employee.objects.get(pk=int(request_id))
#                                 if supervisor != current_user:
#                                     if not check_is_authorized_person(supervisor, current_user):
#                                         raise UnauthorizedException(message="You are Unauthorized to "
#                                                                             "fetch someone else Overtime")
#
#                         if request.method == "POST":
#                             request_body = json.loads(request.body)
#                             if "emp" not in request_body or "request_emp" not in request_body:
#                                 raise Exception("Employee Supervisor must Include")
#                             emp = Employee.objects.get(pk=request_body["emp"]["id"])
#                             request_emp = Employee.objects.get(pk=request_body["request_emp"]["id"])
#                             if emp != current_user:
#                                 if not check_is_authorized_person(current_user, emp):
#                                     raise UnauthorizedException(message="Overtime Request Fail Due "
#                                                                         "to Employee's Supervisor"
#                                                                         " is not this Supervisor")
#                             if request_emp != current_user:
#                                 if not check_is_authorized_person(request_emp, current_user):
#                                     raise UnauthorizedException(message="Overtime Request Fail Due "
#                                                                         "to Employee's Supervisor"
#                                                                         " is not this Supervisor")
#                     if has_number_in_string(request.get_full_path()):
#                         print('overtime modified validation')
#                         print(request.get_full_path()[14:])
#                         ot_id = int(request.get_full_path()[14:])
#                         overtime = Overtime.objects.get(pk=ot_id)
#                         if overtime.emp != current_user or overtime.request_emp != current_user:
#                             raise UnauthorizedException(message="You are Unauthorized to do this")
#                 # Attendance BL
#                 if "/api/attendance" in request.get_full_path():
#                     if request.get_full_path() == "/api/attendance":
#                         if request.method == "POST":
#                             request_body = json.loads(request.body)
#                             emp = Employee.objects.get(pk=request_body["emp"]["id"])
#                             if current_user != emp:
#                                 if not check_is_authorized_person(current_user, emp):
#                                     raise UnauthorizedException("You are Unauthorized To do it")
#                         if request.method == "GET":
#                             emp_id = request.GET.get("emp_id")
#                             if emp_id is None:
#                                 raise Exception("emp_id must include")
#                             emp = Employee.objects.get(int(emp_id))
#                             if emp != current_user:
#                                 if not check_is_authorized_person(current_user, emp):
#                                     raise UnauthorizedException("You are Unauthorized To do it")
#
#                     if has_number_in_string(request.get_full_path()):
#                         at_id = int(request.get_full_path()[16:])
#                         attendance = Attendance.objects.get(pk=at_id)
#                         if attendance.emp != current_user:
#                             if not check_is_authorized_person(current_user, attendance.emp):
#                                 raise UnauthorizedException("You are Unauthorized to do it")
#
#             print('validation finished')
#             response = self.get_response(request)
#             return response
#         except UnauthorizedException as e:
#             return JsonResponse(ApiResponse.unAuthorizedRespose(message=e.message))
#         except Exception as e:
#             return JsonResponse(ApiResponse.internalServerResponse(message=str(e)))
#         # Code to be executed for each request/response after
#         # the view is called.
